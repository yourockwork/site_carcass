<?php

	return [
		'slider_path' => 'slides',
		'theme' => env('THEME','bootstrap4'),
		'banners_path' => 'banners',
		'home_portfolios_count' => 3,
		'home_articles_count' => 3,
		'paginate' => 9,
		'recent_comments' => 3,
		'recent_portfolios' => 3,
		'other_portfolios' => 9,
		'latest_projects' => 'Последние работы',
		'latest_comments' => 'Последние комментарии',
		'articles_img' => [
			'max' => ['width' => 816, 'height' => 282],
			'mini' => ['width' => 55, 'height' => 55]
		],
		'image' => [
			'width' => 1024,
			'height' => 768
		],
	];