<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
		/**
		 * Иногда нужно прямо в шаблоне.blade задавать значения своим переменным.
		 * Сделать это можно прямой вставкой php в blade.шаблоне, но это не элегантно.
		 * Вместо этого создадим свою собственную директиву `setvalue($var,$val)` в AppServiceProvider.php
		 * Она будет доступна в любом blade.шаблоне
		 */

		Blade::directive('setvalue',function ($str){
			list($var,$value) = explode(',',$str);

			return "<?php $var=$value ?>";
		});

		// дамп sql запросов
		/*
		DB::listen(function ($query){
			echo "<h5 style='color:red'>$query->sql</h5>";
		});
		*/
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
