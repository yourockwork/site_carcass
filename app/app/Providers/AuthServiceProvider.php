<?php

namespace App\Providers;

use App\Menu;
use App\Permission;
use App\Policies\MenusPolicy;
use App\Policies\PermissionPolicy;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Article;
use App\Policies\ArticlePolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     * Регистрируем политики безопасности,
	 * связываем  модель с политикой безопасности
     * @var array
     */
    protected $policies = [
		Article::class => ArticlePolicy::class,
		Permission::class => PermissionPolicy::class,
		Menu::class => MenusPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
    	Gate::define('VIEW_ADMIN', function (User $user) {
			return $user->canDo(['VIEW_ADMIN'],true);
		});

		Gate::define('VIEW_ADMIN_ARTICLES', function (User $user) {
			return $user->canDo(['VIEW_ADMIN_ARTICLES'],true);
		});

		Gate::define('EDIT_USERS', function (User $user) {
			return $user->canDo(['EDIT_USERS'],true);
		});

		/*
		Gate::define('admin', function (User $user) {
			dd($user->hasRole(['admin'],true));
			return $user->hasRole(['admin','moderator'],true);
		});
		*/

		// регистрируем подключенные политики
		$this->registerPolicies();
	}
}
