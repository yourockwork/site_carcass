<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sidemenu extends Model
{
	protected $table = 'sidemenus';
	protected $guarded = [];

}