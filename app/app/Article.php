<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
	protected $guarded = [];
	/*
	protected $fillable = [
		'title',
		'head',
		'text',
		'alias',
		'desc',
		'img',
		'created_at',
		'updated_at',
		'user_id',
		'category_id',
		'meta_desc',
		'keywords'
	];
	*/

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function category()
	{
		return $this->belongsTo('App\Category');
	}

	public function comments()
	{
		return $this->hasMany('App\Comment');
	}
}
