<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
	protected $guarded = [];

	public function delete(array $options = [])
	{
		// удаляем дочерние пункты, если они есть
		self::where('parent',$this->id)->delete();

		return parent::delete($options);
	}
}