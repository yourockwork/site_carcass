<?php

namespace App\Repositories;

use App\Article;
use Illuminate\Support\Facades\Gate;

//use Illuminate\Auth\Access\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Intervention\Image\Facades\Image;

class ArticlesRepository extends Repository
{
	public function __construct(Article $articles)
	{
		$this->model = $articles;
	}

	public function one($alias,$attr=[])
	{
		$article = parent::one($alias,$attr);

		// подгружаем все данные из связанных моделей
		if ($article && !empty($attr)){
			$article->load('comments');
			$article->comments->load('user');
		}

		return $article;
	}

	public function updateArticle(Request $request, $article)
	{
		/*
		if (Gate::denies('edit',$this->model)){
			abort(403);
	   }
	   */

		$data = $request->except('_token','image','_method','old_image');

		if (empty($data)){
			return ['error'=>'Заполните все поля'];
		}

		if (empty($data['alias'])){
			$data['alias'] = $this->transliterate($data['title']);
		}

		//dd($this->one($data['alias'],false));
		$result = $this->one($data['alias'],false);
		// ищем запись с таким-же alias
		if ((isset($result->id)) && ($result->id != $article->id)){
			$request->merge(['alias'=>$data['alias']]);
			// сохраним в сессию все ранее указанные поля
			$request->flash();

			return ['error'=>'Псевдоним занят'];
		}

		// если на сервер отправили изображение, то сохраняем со случайно сгенерированным именем
		if ($request->hasFile('image')){
			$image = $request->file('image');
			// используем расширение intervention
			if ($image->isValid()){
				$str = str_random(7);
				$obj = new \StdClass();
				$obj->mini = $str.'_mini.jpg';
				$obj->max = $str.'_max.jpg';
				$obj->path = $str.'.jpg';

				$img = Image::make($image);

				$img->fit(
					Config::get('settings.image')['width'],
					Config::get('settings.image')['height']
				)->save(public_path().'/'.env('THEME').'/img/'.$obj->path);

				$img->fit(
					Config::get('settings.articles_img')['max']['width'],
					Config::get('settings.articles_img')['max']['height']
				)->save(public_path().'/'.env('THEME').'/img/'.$obj->max);

				$img->fit(
					Config::get('settings.articles_img')['mini']['width'],
					Config::get('settings.articles_img')['mini']['height']
				)->save(public_path().'/'.env('THEME').'/img/'.$obj->mini);

				// преобразуем в json формат
				$data['img'] = json_encode($obj);
			}
		}
		// заполняем модель интересующими данными
		$article->fill($data);

		// сохраняем данные в бд
		if ($article->update()){
			return ['status' => 'Обновление завершено'];
		}
	}

	public function deleteArticle(Article $article)
	{
	 	if (Gate::denies('delete',$article)){
	 		abort(403);
		}
		$article->comments()->delete();
	 	if ($article->delete()){
	 		return ['status'=>'Удаление завершено'];
		}
	}

	 public function addArticle(Request $request)
	 {

	 	/*
	 	if (Gate::denies('save',$this->model)){
	 		abort(403);
		}
		*/

		$data = $request->except('_token','image');

	 	if (empty($data)){
	 		return ['error'=>'Заполните все поля'];
		}

		if (empty($data['alias'])){
	 		$data['alias'] = $this->transliterate($data['title']);
		}

		if ($this->one($data['alias'],false)){
			$request->merge(['alias'=>$data['alias']]);
			// сохраним в сессию все ранее указанные поля
			$request->flash();

			return ['error'=>'Псевдоним занят'];
		}

		// если на сервер отправили изображение, то сохраняем со случайно сгенерированным именем
		if ($request->hasFile('image')){
			$image = $request->file('image');
			// используем расширение intervention
			if ($image->isValid()){
				$str = str_random(7);
				$obj = new \StdClass();
				$obj->mini = $str.'_mini.jpg';
				$obj->max = $str.'_max.jpg';
				$obj->path = $str.'.jpg';

				$img = Image::make($image);

				$img->fit(
					Config::get('settings.image')['width'],
					Config::get('settings.image')['height']
				)->save(public_path().'/'.env('THEME').'/img/'.$obj->path);

				$img->fit(
					Config::get('settings.articles_img')['max']['width'],
					Config::get('settings.articles_img')['max']['height']
				)->save(public_path().'/'.env('THEME').'/img/'.$obj->max);

				$img->fit(
					Config::get('settings.articles_img')['mini']['width'],
					Config::get('settings.articles_img')['mini']['height']
				)->save(public_path().'/'.env('THEME').'/img/'.$obj->mini);

				// преобразуем в json формат
				$data['img'] = json_encode($obj);

				// заполняем модель интересующими данными
				$this->model->fill($data);

				// сохраняем данные в бд
				if ($request->user()->article()->save($this->model)){
					return ['status' => 'Статья добавлена'];
				}
			}
		}
	 }

	 public function transliterate($string)
	 {
		 $str = mb_strtolower($string,'UTF-8');
		 $leter_array = [
			 'a' => 'а',
			 'b' => 'б',
			 'v' => 'в',
			 'g' => 'г,ґ',
			 'd' => 'д',
			 'e' => 'е,є,э',
			 'jo' => 'ё',
			 'zh' => 'ж',
			 'z' => 'з',
			 'i' => 'и,і',
			 'ji' => 'ї',
			 'j' => 'й',
			 'k' => 'к',
			 'l' => 'л',
			 'm' => 'м',
			 'n' => 'н',
			 'o' => 'о',
			 'p' => 'п',
			 'r' => 'р',
			 's' => 'с',
			 't' => 'т',
			 'u' => 'у',
			 'f' => 'ф',
			 'kh' => 'х',
			 'ts' => 'ц',
			 'ch' => 'ч',
			 'sh' => 'ш',
			 'shch' => 'щ',
			 '' => 'ъ',
			 'y' => 'ы',
			 '' => 'ь',
			 'yu' => 'ю',
			 'ya' => 'я',
		 ];

		 foreach ($leter_array as $leter => $kyr){
			 $kyr = explode(',',$kyr);
			 $str = str_replace($kyr,$leter,$str);
		 }

		 // любой пробел и любой другой символ, за исключением букв, цифр, -, заменим на -
		 $str = preg_replace('/(\s|[^A-Za-z0-9\-])+/','-',$str);
		 // удаляем - в конце
		 $str = trim($str,'-');

		 return $str;
	 }
 }