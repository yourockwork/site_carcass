<?php

namespace App\Repositories;
 use App\Banner;
 use Illuminate\Support\Facades\Gate;

 class BannersRepository extends Repository
{
	public function __construct(Banner $banner)
	{
		$this->model = $banner;
	}
}