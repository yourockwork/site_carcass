<?php

namespace App\Repositories;
 use App\Block;
 use Illuminate\Support\Facades\Gate;

 class BlocksRepository extends Repository
{
	public function __construct(Block $block)
	{
		$this->model = $block;
	}
}