<?php

namespace App\Repositories;
 use App\Comment;
 use Illuminate\Support\Facades\Gate;

class CommentsRepository extends Repository
{
	public function __construct(Comment $comments)
	{
		$this->model = $comments;
	}
}