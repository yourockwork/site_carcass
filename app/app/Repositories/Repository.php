<?php

namespace App\Repositories;
 use Illuminate\Support\Facades\Config;
 use Illuminate\Support\Facades\Gate;

abstract class Repository{
	// объект модели для оплучения данных
	protected $model;

	/**
	* @return mixed
	* select - список запрашиваемых данных
	* count - Количество элементов
	*/
	public function get($select = '*', $count = false, $pagination = false, $where = false)
	{
		// тянем записи из бд для соответствующей модели
		$builder = $this->model->select($select);

		if ($count){
			$builder->take($count);
		}

		if ($where){
			$builder->where($where['column_name'],$where['column_value']);
		}

		if ($pagination){
			return $this->check($builder->paginate(Config::get('settings.paginate')));
		}

		return $this->check($builder->get());
	}

	public function check($result){

		if ($result->isEmpty()){
			return false;
		}

		$result->transform(function ($item,$key){
			// выполняем декодирование из json формата только после ряда условий
			// т.к. в нашей бд далеко не везде фотки хранятся в формате json
			if (is_string($item->img) && is_object(json_decode($item->img)) && json_last_error() == JSON_ERROR_NONE)
			{
				$item->img = json_decode($item->img);
			}

			return $item;
		});

		return $result;
	}

	/**
	 * @param $alias
	 * Этот метод будет переопределен в дочерних классах
	 * Там мы будем уточнять какие данные необходимо дополнительно-жадно подгружать
	 * @param array $attr
	 * @return mixed
	 */
	public function one($alias,$attr=[])
	{
		return $this->model->where('alias',$alias)->first();
	}
 }