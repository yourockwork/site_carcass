<?php

namespace App\Repositories;
 use App\Page;
 use Illuminate\Support\Facades\Gate;

class PagesRepository extends Repository
{
	public function __construct(Page $page)
	{
		$this->model = $page;
	}
}