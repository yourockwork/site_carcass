<?php

namespace App\Repositories;
 use App\Slider;
 use Illuminate\Support\Facades\Gate;

 class SliderRepository extends Repository
{
	public function __construct(Slider $slider)
	{
		$this->model = $slider;
	}
 }