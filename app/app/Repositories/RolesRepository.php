<?php

namespace App\Repositories;
use App\Role;
use Illuminate\Support\Facades\Gate;

 class RolesRepository extends Repository
{
	public function __construct(Role $role)
	{
		$this->model=$role;
	}
}