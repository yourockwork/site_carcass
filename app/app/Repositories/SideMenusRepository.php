<?php

namespace App\Repositories;
 use App\Sidemenu;
 use Illuminate\Support\Facades\Gate;

class SideMenusRepository extends Repository
{
	public function __construct(Sidemenu $sidemenu)
	{
		$this->model = $sidemenu;
	}
}