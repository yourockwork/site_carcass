<?php

namespace App\Repositories;
use App\Permission;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Request;

class PermissionsRepository extends Repository
{
	protected $rol_rep;

	public function __construct(Permission $permission, RolesRepository $rol_rep)
	{
		$this->model = $permission;
		$this->rol_rep = $rol_rep;
	}

	public function changePermissions($request)
	{
		/*
		if (Gate::denies('change')){
			abort(403);
		}
		*/

		$data = $request->except('_token');
		$roles = $this->rol_rep->get();

		foreach ($roles as $role){
			if($data[$role->id]){
				$role->savePermissions($data[$role->id]);
			}
			else{
				// на случай если у роли не будет ни одной привилегии
				$role->savePermissions([]);
			}
		}

		return ['status' => 'Привилегии обновлены'];
	}
 }