<?php

namespace App\Repositories;
use App\Advantage;
use Illuminate\Support\Facades\Gate;

class AdvantagesRepository extends Repository
{
	public function __construct(Advantage $advantage)
	{
		$this->model = $advantage;
	}
}