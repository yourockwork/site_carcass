<?php

namespace App\Repositories;
 use App\Portfolio;
 use Illuminate\Support\Facades\Gate;

 class PortfolioRepository extends Repository
{
	public function __construct(Portfolio $portfolio)
	{
		$this->model = $portfolio;
	}
 }