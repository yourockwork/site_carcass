<?php

namespace App\Http\Controllers;

use App\Block;
use App\Menu;
use App\Repositories\AdvantagesRepository;
use App\Repositories\ArticlesRepository;
use App\Repositories\BannersRepository;
use App\Repositories\BlocksRepository;
use App\Repositories\MenusRepository;
use App\Repositories\PagesRepository;
use App\Repositories\PortfolioRepository;
use App\Repositories\SliderRepository;
use App\Repositories\SideMenusRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

/**
 * Class IndexController
 * Контроллер главной страницы
 * @package App\Http\Controllers
 */
class IndexController extends SiteController
{
	public function __construct(PagesRepository $pages_rep, AdvantagesRepository $adv_rep, SideMenusRepository $sm_rep, SliderRepository $s_rep, PortfolioRepository $p_rep, ArticlesRepository $a_rep, BannersRepository $banners_rep)
	{
		$m_rep = new MenusRepository(new Menu());
		$blocks_rep = new BlocksRepository(new Block());
		parent::__construct($m_rep, $blocks_rep);

		$this->page_rep = $pages_rep;
		$this->banners_rep = $banners_rep;
		$this->adv_rep = $adv_rep;
		$this->sm_rep = $sm_rep;
		$this->p_rep = $p_rep;
		$this->s_rep = $s_rep;
		$this->a_rep = $a_rep;
		//$this->bar = false;
		// .index, точка - это разделитель для каталогов
		$this->template = env('THEME').'.index';
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$sliderItems = $this->getSliders();
		$slider = view(env('THEME').'.slider')->with('slider',$sliderItems)->render();
		$this->vars = array_add($this->vars,'slider',$slider);

		$portfolioItems = $this->getPortfolio();
		$portfolio = view(env('THEME').'.portfolio')->with('portfolio',$portfolioItems)->render();
		$this->vars = array_add($this->vars,'portfolio',$portfolio);

		$advantagesItems = $this->getAdvantages();
		$advantages = view(env('THEME').'.advantages')->with('advantages',$advantagesItems)->render();
		$this->vars = array_add($this->vars,'advantages',$advantages);

		$articlesBlock = $this->getArticlesBlock();
		$sideMenu = $this->getSideMenu();
		$banners = $this->getBanners();
		$slogan = $this->blocks_rep->one('slogan');

		$this->contentRightBar = view(env('THEME').'.indexBar')->with(['articles' => $articlesBlock, 'sidemenu' => $sideMenu, 'banners' => $banners, 'slogan' => $slogan])->render();
		if ($this->contentRightBar){
			$rightBar = view(env('THEME').'.rightBar')->with('content_rightBar',$this->contentRightBar)->render();
			$this->vars = array_add($this->vars,'rightBar',$rightBar);
		}

		$this->keywords = $this->getPage('about_us')->keywords;
		$this->meta_desc = $this->getPage('about_us')->meta_description;
		$this->pageTitle = $this->getPage('about_us')->title;
		$this->pageHead = $this->getPage('about_us')->head;
		$this->pageContent= $this->getPage('about_us')->content;

    	return $this->renderOutput();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

	protected function getPortfolio()
	{
		$portfolio = $this->p_rep->get('*',Config::get('settings.home_portfolios_count'));

		return $portfolio;
	}

    protected function getSliders()
	{
		$sliders = $this->s_rep->get();
		$sliders->transform(function ($item,$key){
			$item->img = Config::get('settings.slider_path').'/'.$item->img;

			// функция должна возвратить обновленный элемент коллекции, тот который и вызывает
			return $item;
		});

		return $sliders;
	}

	protected function getAdvantages()
	{
		$advantages = $this->adv_rep->get();

		return $advantages;
	}
}