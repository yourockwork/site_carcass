<?php

namespace App\Http\Controllers;

use App\Block;
use App\Repositories\BlocksRepository;
use App\Repositories\MenusRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Lavary\Menu\Menu;
use function Sodium\add;

/**
 * Class SiteController
 * Родительский контроллер
 * @package App\Http\Controllers
 */
class SiteController extends Controller
{
	// тут логика хранения портфолио
	protected $p_rep;
	// тут логика хранения слайдера
	protected $s_rep;
	// тут логика хранения categories
	protected $cat_rep;
	// тут логика хранения advantages
	protected $adv_rep;
	// тут логика хранения блоков сайта, таблица blocks
	protected $blocks_rep;
	// тут логика хранения слайдера
	protected $banners_rep;
	// тут логика хранения статей
	protected $a_rep;
	// тут логика хранения menu
	protected $m_rep;
	// тут логика хранения side menu
	protected $sm_rep;
	// тут логика хранения информаици о страницах сайта
	protected $page_rep;
	// имя шаблона для отображения информации
	protected $template;
	// массив переменных для шаблона
	protected $vars = [];
	// указывает есть ли sidebar
	protected $bar = true;
	protected $keywords;
	protected $meta_desc;
	protected $pageHead;
	protected $pageTitle;
	protected $pageContent;
	// информация в левом и правом sidebar
	protected $contentRightBar = false;
	//protected $contentLeftBar = false;

	public function __construct(MenusRepository $m_rep, BlocksRepository $blocks_rep)
	{
		$this->m_rep = $m_rep;
		$this->blocks_rep = $blocks_rep;
	}

	/**
	 * Возвращает конкретный сформированный вид (шаблон html)
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	protected function renderOutput(){
		$menu = $this->getMenu();
		$navbar = view(env('THEME').'.navbar')->with('menu', $menu)->render();
		$this->vars = array_add($this->vars,'navbar',$navbar);

		$email = view(env('THEME').'.emailBlock')->with('email', $this->getBlock('email'))->render();
		$this->vars = array_add($this->vars,'email',$email);

		$address = view(env('THEME').'.addressBlock')->with('address', $this->getBlock('address'))->render();
		$this->vars = array_add($this->vars,'address',$address);

		$phone = view(env('THEME').'.phoneBlock')->with('phone', $this->getBlock('phone'))->render();
		$this->vars = array_add($this->vars,'phone',$phone);

		$slogan = view(env('THEME').'.sloganBlock')->with('slogan', $this->getBlock('slogan'))->render();
		$this->vars = array_add($this->vars,'slogan',$slogan);

		$copyright = view(env('THEME').'.copyrightBlock')->with('copyright', $this->getBlock('copyright'))->render();
		$this->vars = array_add($this->vars,'copyright',$copyright);

		$this->vars = array_add($this->vars,'bar',$this->bar);
		$this->vars = array_add($this->vars,'keywords',$this->keywords);
		$this->vars = array_add($this->vars,'meta_desc',$this->meta_desc);
		$this->vars = array_add($this->vars,'pageHead',$this->pageHead);
		$this->vars = array_add($this->vars,'pageTitle',$this->pageTitle);
		$this->vars = array_add($this->vars,'pageContent',$this->pageContent);

		return view($this->template)->with($this->vars);
	}

	/**
	 * Возвращает конкретный сформированный вид (шаблон html)
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getMenu(){
		$menu = $this->m_rep->get();
		// подготавливаем объект меню пригодный для использования в шаблоне
		$lavaryMenu = new \Lavary\Menu\Menu();
		$menuBuilder = $lavaryMenu->make('TopNav', function ($m) use ($menu){
			foreach ($menu as $item){
				if ($item->parent == 0){
					$m->add($item->title,$item->path)->id($item->id);
				}
				else{
					// ищем родителя,
					// find вернет интересующий пункт по идентификатору
					if ($m->find($item->parent)){
						// нашли родителя добавим, к нему дочерний пункт
						$m->find($item->parent)->add($item->title,$item->path)->id($item->id);
					}
				}
			}
		});

		return $menuBuilder;
	}

	/**
	 * Возвращает конкретный сформированный вид (шаблон html)
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getSideMenu(){
		$menu = $this->sm_rep->get();
		// подготавливаем объект меню пригодный для использования в шаблоне
		$lavaryMenu = new \Lavary\Menu\Menu();
		$menuBuilder = $lavaryMenu->make('SideNav', function ($m) use ($menu){
			foreach ($menu as $item){
				if ($item->parent == 0){
					$m->add($item->title,$item->path)->id($item->id);
				}
				else{
					// ищем родителя,
					// find вернет интересующий пункт по идентификатору
					if ($m->find($item->parent)){
						// нашли родителя добавим, к нему дочерний пункт
						$m->find($item->parent)->add($item->title,$item->path)->id($item->id);
					}
				}
			}
		});

		return $menuBuilder;
	}

	protected function getBanners()
	{
		$banners = $this->banners_rep->get();
		$banners->transform(function ($item,$key){
			$item->img = Config::get('settings.banners_path').'/'.$item->img;

			return $item;
		});

		return $banners;
	}

	protected function getArticlesBlock()
	{
		$articles = $this->a_rep->get(['title','created_at','img','alias','created_at'], Config::get('settings.home_articles_count'));

		return $articles;
	}

	protected function getPage($alias){
		return $this->page_rep->one($alias);
	}

	protected function getBlock($alias)
	{
		$block = $this->blocks_rep->one($alias);

		return $block;
	}
}