<?php

namespace App\Http\Controllers;

use App\Block;
use App\Category;
use App\Repositories\ArticlesRepository;
use App\Repositories\BannersRepository;
use App\Repositories\BlocksRepository;
use App\Repositories\MenusRepository;
use App\Repositories\PagesRepository;
use App\Repositories\SideMenusRepository;
use App\Menu;

class ArticlesController extends SiteController
{
	public function __construct(Category $cat_rep, PagesRepository $pages_rep, SideMenusRepository $sm_rep, ArticlesRepository $a_rep, BannersRepository $banners_rep)
	{
		$m_rep = new MenusRepository(new Menu());
		$blocks_rep = new BlocksRepository(new Block());
		parent::__construct($m_rep, $blocks_rep);

		$this->cat_rep = $cat_rep;
		$this->page_rep = $pages_rep;
		$this->banners_rep = $banners_rep;
		$this->sm_rep = $sm_rep;
		$this->a_rep = $a_rep;

		// .index, точка - это разделитель для каталогов
		$this->template = env('THEME').'.articles';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index($cat_alias = false)
	{
		//dd($this->getArticles($cat_alias));
		$this->vars = array_add($this->vars,'articles', view(env('THEME').'.articles_content')->with(['articles' => $this->getArticles($cat_alias), 'categories' => $this->getCategories()])->render());
		$this->contentRightBar = view(env('THEME').'.indexBar')->with(
			[
				'articles' => $this->getArticlesBlock(),
				'sidemenu' => $this->getSideMenu(),
				'banners' => $this->getBanners(),
				'slogan' => $this->blocks_rep->one('slogan')
			])->render();

		if ($this->contentRightBar){
			$rightBar = view(env('THEME').'.rightBar')->with('content_rightBar',$this->contentRightBar)->render();
			$this->vars = array_add($this->vars,'rightBar',$rightBar);
		}

		$this->keywords = $this->getPage('articles_page')->keywords;
		$this->meta_desc = $this->getPage('articles_page')->meta_description;
		$this->pageTitle = $this->getPage('articles_page')->title;
		$this->pageHead = $this->getPage('articles_page')->head;
		$this->pageContent= $this->getPage('articles_page')->content;

		return $this->renderOutput();
	}

	public function show($alias=false){
		$article = $this->a_rep->one($alias,['comments' => true]);
		$articleGroup = $article->comments->groupBy('parent_id');
		//dd($article->comments->groupBy('parent_id'));
		if ($article){
			$article->img = json_decode($article->img);
		}
		$this->vars = array_add($this->vars,'articles', view(env('THEME').'.article_content')->with(['article' => $article, 'articleGroup' => $articleGroup])->render());
		$this->contentRightBar = view(env('THEME').'.indexBar')->with(
			[
				'articles' => $this->getArticlesBlock(),
				'sidemenu' => $this->getSideMenu(),
				'banners' => $this->getBanners(),
				'slogan' => $this->blocks_rep->one('slogan')
			])->render();
		if ($this->contentRightBar){
			$rightBar = view(env('THEME').'.rightBar')->with('content_rightBar',$this->contentRightBar)->render();
			$this->vars = array_add($this->vars,'rightBar',$rightBar);
		}

		if(isset($article->id)) {
			$this->pageTitle = $article->title;
			$this->pageHead = $article->head;
			$this->meta_desc = $article->meta_desc;
			$this->keywords = $article->keywords;
			$this->pageContent= $article->content;
		}

		return $this->renderOutput();
	}

	protected function getArticles($alias)
	{
		$where = false;
		if ($alias){
			$id = Category::select('id')->where('alias',$alias)->first()->id;
			$where = ['column_name' => 'category_id', 'column_value' => $id];
		}
		//$articles = $this->a_rep->get(['title','desc','created_at','user_id','category_id','img','alias','created_at','user_id'],false,true,false);
		$articles = $this->a_rep->get('*',false,false, $where);
		// механизм жадной загрузки данных их связанных моделей
		// этот механизм сразу подгружает информацию из связанных моделей в данную коллекцию
		if ($articles){
			$articles->load('user','category','comments');
		}

		return $articles;
	}

	protected function getCategories()
	{
		$categories = $this->cat_rep->get();

		return $categories;
	}
}