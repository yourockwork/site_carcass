<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	use Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [];

	/*
	protected $fillable = [
		'name', 'email', 'password','login'
	];
	*/


	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	public function article(){
		// связь один ко многим
		// один юзер может добавить много статей
		return $this->hasMany('App\Article');
	}

	public function comments(){
		// связь один ко многим
		// один юзер может добавить много комментариев
		return $this->hasMany('App\Comment');
	}

	public function roles(){
		// многие ко многим
		// users связана с roles через таблицу role_user
		return $this->belongsToMany('App\Role','role_user');
	}

	/**
	 * Проверяем наличие привилегий у роли пользователя
	 * $require = true - для доступа должны быть все привилегии
	 * $require = false - для доступа достаточно хотя-бы одной привилегии
	 *
	 * @param $permission
	 * @param bool $require
	 * @return bool
	 */
	public function canDo($permission, $require = false) {
		if(is_array($permission)) {
			foreach($permission as $permName) {
				$permName = $this->canDo($permName);
				if($permName && !$require) {
					return true;
				}
				else if(!$permName  && $require) {
					return false;
				}
			}

			return  $require;
		}
		else {
			foreach($this->roles as $role) {
				foreach($role->perms as $perm) {
					if(str_is($permission,$perm->name)) {
						return true;
					}
				}
			}
		}
	}

	/**
	 * Проверяем наличие ролей у пользователя
	 * $require = true - для доступа должны быть все роли
	 * $require = false - для доступа достаточно хотя-бы одной роли
	 *
	 * @param $name
	 * @param bool $require
	 * @return bool
	 */
	public function hasRole($name, $require = false)
	{
		if (is_array($name)) {
			foreach ($name as $roleName) {
				$hasRole = $this->hasRole($roleName);
				//echo var_dump($hasRole)." ничего<br>";
				if ($hasRole && !$require) {
					return true;
				} elseif (!$hasRole && $require) {
					//echo "завершаю работу<br>";
					return false;
				}
			}
			return $require;
		}
		else
		{
			foreach ($this->roles as $role) {
				//echo "ищу роль<br>";
				if ($role->name == $name) {
					return true;
				}
			}
		}
		//echo "не нашел роль<br>";
		return false;
	}
}