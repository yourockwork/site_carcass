<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
	// если таблица автоматически не привяжется к модели то можно сделать это вручную
	//protected $table = 'portfolio';
	protected $guarded = [];

	public function filter(){
		return $this->belongsTo('App/Filter','filter_alias','alias');
	}
}