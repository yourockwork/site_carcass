<?php

namespace App\Exceptions;

use App\Block;
use App\Http\Controllers\SiteController;
use App\Menu;
use App\Repositories\BlocksRepository;
use App\Repositories\MenusRepository;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
    	// делаем предварительную проверку исключеий
		// нам нужно выбрать макет страницы с ошибкой у активного шаблона вместо стандартного из папки resource/views/errors
		if ($this->isHttpException($exception)){
			$statusCode = $exception->getStatusCode();
			// на случай если будет нужно на страничке ошибок вывести общие блоки для других страниц: меню, контакты и и т.д.
			// $obj = new SiteController(new MenusRepository(new Menu()),new BlocksRepository(new Block()));
			// $navbar = view(env('THEME').'.navbar')->with('menu',$obj->getMenu())->render();

			// пишем в логи информацию о ошибочно запрашиваемой пользователем странице
			Log::alert('Страница не найдена - '. $request->url());

			switch ($statusCode){
				case '404':

					return response()->view(env('THEME').'.errors.404',[
						'meta_desc' => '',
						'pageHead'=>'Ошибка: 404 - Страница не найдена',
						'pageTitle'=>'',
						'pageContent'=>false,
						'keywords' => '',
						'bar' => false,
						//'navbar' => $navbar
					]);
			}
		}
        return parent::render($request, $exception);
    }
}
