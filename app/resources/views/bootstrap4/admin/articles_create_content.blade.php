{{--
	Этот макет используется для обновления и добавления информациии, поэтому тут используются условные операторы и переменная, которой тут быть не должно - $article, как в случае добавления материала
--}}

<div id="content-page" class="content group">
	<div class="hentry group">
		{{-- в случае редактирования статьи , вместе с маршрутом мы должны передать псевдоним редактируемой статьи --}}
		{!! Form::open(['url' => (isset($article->id)) ? route('admin.articles.update',['articles'=>$article->alias]) : route('admin.articles.store'),'class'=>'b-feedback m-b__15px','method'=>'POST','enctype'=>'multipart/form-data']) !!}

			<div class="form-group">
				{!! Form::text('title',isset($article->title) ? $article->title  : old('title'), ['class'=>'form-control','placeholder'=>'Заголовок - H1 страницы']) !!}
			</div>

			<div class="form-group">
				{!! Form::text('head',isset($article->head) ? $article->head  : old('head'), ['class'=>'form-control','placeholder'=>'Title страницы']) !!}
			</div>

			<div class="form-group">

				{!! Form::text('alias',isset($article->alias) ? $article->alias  : old('alias'), ['class'=>'form-control','placeholder'=>'Введите псевдоним страницы']) !!}
			</div>

			<div class="form-group">

				{!! Form::text('keywords', isset($article->keywords) ? $article->keywords  : old('keywords'), ['class' => 'form-control','placeholder'=>'Ключевые слова']) !!}
			</div>

			<div class="form-group">
				{!! Form::text('meta_desc', isset($article->meta_desc) ? $article->meta_desc  : old('meta_desc'), ['class' => 'form-control','placeholder'=>'Мета описание']) !!}
			</div>

			<div class="form-group">

				{!! Form::textarea('desc', isset($article->desc) ? $article->desc  : old('desc'), ['id'=>'editor','class' => 'form-control','placeholder'=>'Краткое описание']) !!}
			</div>

			<div class="form-group">
				{!! Form::textarea('text', isset($article->text) ? $article->text  : old('text'), ['id'=>'editor2','class' => 'form-control','placeholder'=>'Полное описание']) !!}
			</div>

			<div class="form-group">
				{!! Form::select('category_id',$categories,isset($article->category_id) ? $article->category_id  : '',['class' => 'form-control inpselect']) !!}
			</div>

			{{-- если шаблон используется для обновления --}}
			@if(isset($article->img->path))
				<div class="form-group">
					<label>
						<span class="label">Изображения материала:</span>
					</label>
					{{ Html::image(asset(config('settings.theme')).'/img/'.$article->img->path,'',['style'=>'width:400px']) }}
					{!! Form::hidden('old_image',$article->img->path) !!}
				</div>
			@endif

			{{-- если шаблон используется для добавления --}}
			<div class="form-group">
				<label for="exampleFormControlFile1">Изображение:</label>
				{!! Form::file('image', ['class' => 'filestyle','data-buttonText'=>'Выберите изображение','data-buttonName'=>"btn-primary",'data-placeholder'=>"Файла нет"]) !!}
				@if(isset($article->img->path))
					{!! Form::hidden('old_image',$article->img->path) !!}
				@endif
			</div>

			{{-- маршрут update работает с запросом put, который в rest используется для редактирования записей --}}
			@if(isset($article->id))
				<input type="hidden" name="_method" value="PUT">
			@endif

			{!! Form::button('Сохранить', ['class' => 'btn btn-success','type'=>'submit']) !!}

		{!! Form::close() !!}

		<script>
            CKEDITOR.replace( 'editor' );
            CKEDITOR.replace( 'editor2' );
		</script>
	</div>
</div>