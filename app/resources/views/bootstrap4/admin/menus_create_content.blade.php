<div id="content-page" class="content group">
	{!! Form::open(['url' => (isset($menu->id)) ? route('admin.menus.update',['menus'=>$menu->id]) : route('admin.menus.store'),'class'=>'b-feedback m-b__15px','method'=>'POST','enctype'=>'multipart/form-data']) !!}

	<div class="form-group">
		{!! Form::text('title',isset($menu->title) ? $menu->title  : old('title'), ['class' => 'form-control','placeholder'=>'Введите название страницы']) !!}
	</div>

	<div class="form-group">
		{!! Form::select('parent', $menus, isset($menu->parent) ? $menu->parent : null, ["class" => "form-control inpselect"]) !!}
	</div>
	<p>Выберите тип меню:</p>

	<div class="hentry group">
		<div class="b-accordion">

			<div class="b-accordion__item js-accordion__item">
				<p>{!! Form::radio('type', 'customLink',(isset($type) && $type == 'customLink') ? TRUE : FALSE,['class' => 'radioMenu']) !!}
					<span class="b-accordion__header">Пользовательская ссылка:</span>
				</p>
				<label for="name-contact-us">
					<span class="sublabel">Путь для ссылки</span><br />
				</label>
				<div class="form-group">
					<span class="add-on"><i class="icon-user"></i></span>
					{!! Form::text('custom_link',(isset($menu->path) && $type=='customLink') ? $menu->path  : old('custom_link'), ['class'=>"form-control", 'placeholder'=>'Введите название страницы']) !!}
				</div>
			</div>

			<div class="b-accordion__item js-accordion__item">
				<p>
					{!! Form::radio('type', 'blogLink',(isset($type) && $type == 'blogLink') ? TRUE : FALSE,['class' => 'radioMenu']) !!}
					<span class="b-accordion__header">Раздел Блог:</span>
				</p>
				<div class="form-group">
					<label for="name-contact-us">
						<span class="sublabel">Ссылка на категорию блога</span><br />
					</label>
					@if($categories)
						{!! Form::select('category_alias',$categories,(isset($option) && $option) ? $option :FALSE,['class'=>'form-control inpselect']) !!}
					@endif
				</div>
				<div class="form-group">
					<label for="name-contact-us">
						<span class="sublabel">Ссылка на материал блога</span><br />
					</label>
					{!! Form::select('article_alias', $articles, (isset($option) && $option) ? $option :FALSE, ['class'=>'form-control inpselect', 'placeholder' => 'Не используется']) !!}
				</div>
			</div>

		</div>
		<br />
		@if(isset($menu->id))
			<input type="hidden" name="_method" value="PUT">
		@endif
		{!! Form::button('Сохранить', ['class' => 'btn btn-success','type'=>'submit']) !!}

		{!! Form::close() !!}
	</div>
</div>
<script>
	$('.js-accordion__item').on('click', function(){
	    $(this).find('input.radioMenu').prop('checked',true);
	})
</script>