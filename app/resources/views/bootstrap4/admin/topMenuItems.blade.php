@foreach($items as $item)
	<li class="{{ ($item->hasChildren()) ? 'dropdown' : '' }} {{ (URL::current() == $item->url()) ? 'active' : '' }}">
		@if($item->hasChildren())
			<a href="#" class="dropdown-toggle {{ (URL::current() == $item->url()) ? 'active' : '' }}" data-toggle="dropdown" aria-expanded="false">{{$item->title}}<b class="caret"></b></a>
			<ul class="dropdown-menu">
				{{-- рекурсивный вызов шаблона, но уже передаем только дочерние пункты--}}
				@include(env('THEME').'.admin.topMenuItems',['items' => $item->children()])
			</ul>
		@else
			<a href="{{$item->url()}}" class="{{ (URL::current() == $item->url()) ? 'active' : '' }}">{{$item->title}}</a>
		@endif
	</li>
@endforeach
