<div id="content-page" class="content group">
	<div class="hentry group">

		{!! Form::open(['url' => (isset($user->id)) ? route('admin.users.update',['users'=>$user->id]) :route('admin.users.store'),'class'=>'contact-form','method'=>'POST','enctype'=>'multipart/form-data']) !!}

			<div class="form-group">
				{!! Form::text('name',isset($user->name) ? $user->name  : old('name'), ['class' => 'form-control', 'placeholder'=>'Имя пользователя']) !!}
			</div>

			<div class="form-group">
				{!! Form::text('email',isset($user->email) ? $user->email  : old('email'), ['class' => 'form-control', 'placeholder'=>'Email']) !!}
			</div>

			<div class="form-group">
				{!! Form::password('password', ['class' => 'form-control', 'placeholder'=>'password']) !!}
			</div>

			<div class="form-group">
				{!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder'=>'confirm password']) !!}
			</div>

			<div class="form-group">
				{!! Form::select('role_id', $roles, (isset($user)) ? $user->roles()->first()->id : null, ['class' => 'form-control inpselect']) !!}
			</div>

			@if(isset($user->id))
				<input type="hidden" name="_method" value="PUT">
			@endif

			{!! Form::button('Сохранить', ['class' => 'btn btn-success','type'=>'submit']) !!}
		{!! Form::close() !!}

	</div>
</div>