@if($menus)
	<table>
		<thead>
			<tr>
				<th>Заголовок</th>
				<th>Ссылка</th>
				<th>Удалить</th>
			</tr>
		</thead>
		<tbody>
			@include(env('THEME').'.admin.custom-menu-items',['items' => $menus->roots(), 'paddingLeft' => ''])
		</tbody>
	</table>
	{!! HTML::link(route('admin.menus.create'),'Добавить пункт меню',['class' => 'btn btn-success']) !!}
@endif