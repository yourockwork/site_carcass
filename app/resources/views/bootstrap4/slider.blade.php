@setvalue($var,7)
@if(count($slider)>0)
    <div class="carousel slide" id="myCarousel" data-ride="carousel">
        <ol class="carousel-indicators hidden-xs">
            @for ($i=0; $i < count($slider); $i++)
                <li class="{{ ($i==0) ? 'active' : '' }}" data-target="#myCarousel" data-slide-to="{{ $i }}"></li>
            @endfor
        </ol>
        <div class="carousel-inner">
            @foreach($slider as $i => $slide)
                <div class="item {{ ($i==0) ? 'active' : '' }}">
                    <img src="{{asset(env('THEME'))}}/img/{{ ($slide->img) ? $slide->img : 'slide.png' }}" alt="" style="width:100%;">
                    <div class="container">
                        <div class="carousel-caption">
                            <h1 class="hidden-xxs">{{$slide->title}}</h1>
                            <p>{{$slide->desc}}</p>
                            <p><a class="btn btn-success" href="{{$slide->link}}">More</a></p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span><span class="sr-only">Prev</span></a><a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span><span class="sr-only">Next</span></a>
    </div>
@endif