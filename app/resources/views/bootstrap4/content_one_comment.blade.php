<li id="li-comment-{{$data['id']}}" class="comment even bg-success">
	<div id="comment-{{$data['id']}}" class="comment-container">
		<div class="comment-author vcard">
			<img alt="" src="{{asset(env('THEME'))}}/img/no-avatar.png" class="avatar" />
			{{-- <cite class="fn">{{ $data['name'] }}</cite> --}}
			<p>{{$data['name']}}</p>
		</div>
		<div class="comment-meta commentmetadata">
			<div class="intro">
				@if(is_object($data['created_at']))
					<div class="commentDate">
						<a href="#comment-{{$data['id']}}">{{ $data['created_at']->format('F d, Y \a\t H:i')}}</a>
						@if($data['parent_id'] && $data['parent_id']>0)
							в ответ на <a href="#comment-{{$data['parent_id']}}"> комментарий</a>
						@endif
					</div>
				@endif
				<div class="commentNumber">#</div>
			</div>
			<div class="comment-body">
				<p>{{$data['text']}}</p>
			</div>
		</div>
	</div>
</li>
