@if($address)
    <div class="b-information b-information--address">
        {!! $address->content !!}
    </div>
@endif