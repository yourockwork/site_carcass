{{-- в данном шаблоне мы переопределяем все секции --}}
@extends(env('THEME').'.layouts.site')

@section('email')
	{!! $email !!}
@endsection

@section('address')
	{!! $address !!}
@endsection

@section('phone')
	{!! $phone !!}
@endsection

@section('copyright')
	{!! $copyright !!}
@endsection

@section('navbar')
	{!! $navbar !!}
@endsection

@section('bar')
	{!! $rightBar !!}
@endsection

@section('articles')
	{!! $articles !!}
@endsection