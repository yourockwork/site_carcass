@extends(env('THEME').'.layouts.site')
{{--
@section('navbar')
	{!! ($navbar)?$navbar:''!!}
@endsection
--}}
@section('error_content')
	<div class="b-error">
		<div class="b-error__number">
			<p>404</p>
		</div>
		<div class="b-error__text">
			<div class="b-header3">
				<h3 class="b-h3">Error 404</h3>
				<div class="b-h3__sub"></div>
			</div>
			<p>Page not found</p>
			<a class="btn btn-success" href="{{route('home')}}">На главную</a>
		</div>
	</div>
@endsection