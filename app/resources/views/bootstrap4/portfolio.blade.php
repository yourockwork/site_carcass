@if($portfolio)
    <div class="b-header3">
        <h3 class="b-h3">
            Portfolio
        </h3>
        <a class="b-h3__sub" href="service.html" target="_blank">
            Show all
        </a>
    </div>
    <div class="row flex-row">
        @foreach($portfolio as $item)
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="b-news">
                    <div class="b-news__img">
                        <a class="b-news__link b-news--no_img" href="news_page.html">
                            <img class="img-responsive" src="{{asset(env('THEME'))}}/img/{{ ($item->img) ? $item->img->path : 'no-img.jpg' }}">
                        </a>
                    </div>
                    <div class="b-news__content">
                        <div class="b-news__name">
                            {{$item->title}}
                        </div>
                        <div class="b-news__anons">
                            {{str_limit($item->text,200)}}

                        </div>
                        <a class="b-news__more" href="{{route('portfolios.show',['alias' => $item->alias])}}">
                            More
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endif