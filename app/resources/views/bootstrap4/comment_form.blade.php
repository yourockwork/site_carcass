{{--
<div class="b-header3">
	<h3 class="b-h3" id="reply-title">Form<small><a href="#respond" rel="nofollow" style="display:none;" id="cancel-comment-reply-link">Cancel reply</a></small></h3>
</div>
<div id="respond">
	<form class="b-feedback m-b__15px" id="commentform">
		@if(!Auth::check())

		@endif
		<input id="comment_post_ID" type="hidden" name="comment_post_ID" value="{{$article->id}}">
		<input id="comment_post_ID" type="hidden" name="comment_parent" value="">
		<div class="input-group">
			<span class="input-group-addon min-width__40px">
				<i class="fa fa-user"></i>
			</span>
			<input id="author" class="form-control" placeholder="Имя" name="author">
		</div>
		<div class="input-group">
			<span class="input-group-addon min-width__40px">
			<i class="fa fa-envelope"></i>
			</span>
			<input id="email" class="form-control" placeholder="email" name="email">
		</div>
		<div class="input-group">
			<span class="input-group-addon min-width__40px">
				<i class="fa fa-link"></i>
			</span>
			<input id="url" class="form-control" placeholder="website" name="site">
		</div>
		<div class="form-group">
			<textarea class="form-control" placeholder="Сообщение" name="comment" id="comment"></textarea>
		</div>
		<div class="form-group">
			<div class="b-form-check">
				<input class="form-check-input" type="checkbox" id="privacyCheck">
				<label class="form-check-label" for="privacyCheck">Я соглашаюсь с обработкой моих персональных данных</label>
			</div>
		</div>
		<button class="btn btn-success">Отправить</button>
	</form>
</div>--}}
<ol class="trackbacklist"></ol>
<div id="respond">
	<div class="b-header3">
		<h3 id="reply-title" class="b-h3">Leave a<span>Reply</span>
			<small>
				<a id="cancel-comment-reply-link" rel="nofollow" href="#respond" style="display:none;">Cancel reply</a>
			</small>
		</h3>
	</div>
	<form id="commentform" class="b-feedback m-b__15px" action="{{route('comment.store')}}" method="post">
		{{csrf_field()}}
		<input id="comment_post_ID" name="article_id" type="hidden" value="{{$article->id}}" />
		<input id="comment_parent" name="parent_id" type="hidden" value="0" />
		@if(!Auth::check())
			<div class="input-group">
				<p class="comment-form-author">
					<input class="form-control" id="name" name="name" type="text" value="" size="30" placeholder="Имя" aria-required="true">
				</p>
			</div>
			<div class="input-group">
				<p class="comment-form-email">
					<input class="form-control" id="email" name="email" type="text" value="" size="30" aria-required="true" placeholder="Email">
				</p>
			</div>
			<div class="input-group">
				<p class="comment-form-url">
					<input class="form-control" id="site" name="site" type="text" value="" size="30" placeholder="Website">
				</p>
			</div>
		@endif
		<div class="form-group">
			<p class="comment-form-comment">
				<textarea class="form-control" id="comment" name="text" cols="45" rows="8" placeholder="Message"></textarea>
			</p>
		</div>
		<div class="clear"></div>
		<p class="form-submit">
			<input class="btn btn-success" id="submit" type="submit" value="Post Comment">
		</p>
	</form>
</div>