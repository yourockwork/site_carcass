@if($sidemenu)
	<div class="list-group">
		{{-- вызываем и передаем в шаблон только родительские пункты меню --}}
		@include(env('THEME').'.sideMenuItems',['items' => $sidemenu->roots()])
	</div>
@endif