<div class="row flex-row">
    @if($articles)
        @foreach($articles as $article)
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="b-news">
                    <div class="b-news__img">
                        <a class="b-news__link b-news--no_img" href="{{route('articles.show',['alias' => $article->alias])}}">
                            <img class="img-responsive" src="{{asset(env('THEME'))}}/img/{{ ($article->img) ? $article->img->path : 'no-img.jpg' }}">
                        </a>
                    </div>
                    <div class="b-news__content">
                        <div class="b-news__name">{{$article->title}}</div>
                        <div class="b-news__anons">
                            {{$article->desc}}
                            <br>
                            <b>
                                Author:
                            </b>
                            <span>{{$article->user->name}}</span>
                            <br>
                            <b>
                                Category:
                            </b>
                            <a href="{{route('articlesCat',['cat_alias' => $article->category->alias])}}">{{$article->category->title}}</a>
                            <br>
                            <b>
                                {{count($article->comments)}} {{Lang::choice('ru.comments',count($article->comments))}}
                            </b>
                            {{--
                            <div class="b-filters">
                                <a class="b-filter" href="{{route('articlesCat',['cat_alias' => $article->category->alias])}}">{{$article->category->title}}</a>
                            </div>
                            --}}
                            <div class="b-date-block"><b>Date:</b><span>{{$article->created_at->format('d D. M - Y')}}</span></div>
                        </div>
                        <a class="b-news__more" href="{{route('articles.show',['alias' => $article->alias])}}">{{Lang::get('ru.read_more')}}</a>
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <p>{{Lang::get('ru.articles_no')}}</p>
    @endif
</div>

{{--
<nav class="text-center">
    {{$articles->links()}}
</nav>
--}}
