@if($article)
    <div class="b-content">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-5 col-xs-12">
                <div class="b-news-page__img">
                    <img class="img-responsive" src="{{asset(env('THEME'))}}/img/{{ ($article->img) ? $article->img->path : 'no-img.jpg' }}">
                </div>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-7 col-xs-12">
                {!! $article->text !!}
                <div class="b-attr">
                    <p><b>Filter:</b><span>{{$article->category->title}}</span></p>
                    <p><b>Author:</b><span>{{$article->user->name}}</span></p>
                    <p><b>Date:</b><span>{{$article->created_at->format('D d. M - Y')}}</span></p>
                </div>
            </div>
        </div>
        @if(count($article->comments)>0)
            <div class="row">
                <div class="col-lg-12">
                    <div id="comments">
                        <div class="b-header2">
                            <h2 class="b-h2" id="comments-title">{{count($article->comments)}} {{Lang::choice('ru.comments',count($article->comments))}}</h2>
                            <div class="b-h2__sub">leave a reply</div>
                        </div>
                        <ol class="commentlist group">
                            @foreach($articleGroup as $k => $comments)
                                @if($k !== 0)
                                    @break
                                @endif
                                @include(env('THEME').'.comments',['comments' => $comments])
                            @endforeach
                        </ol>
                        @include(env('THEME').'.comment_form')
                    </div>
                </div>
            </div>
        @endif
        <!--nav aria-label="...">
            <ul class="pager">
                <li class="previous"><a href="news.html">Prev</a></li>
                <li class="next"><a href="#">Next</a></li>
            </ul>
        </nav-->
    </div>
@endif
