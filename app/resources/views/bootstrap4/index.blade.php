{{-- в данном шаблоне мы переопределяем все секции --}}
@extends(env('THEME').'.layouts.site')

@section('email')
	{!! $email !!}
@endsection

@section('address')
	{!! $address !!}
@endsection

@section('phone')
	{!! $phone !!}
@endsection

@section('copyright')
	{!! $copyright !!}
@endsection

@section('navbar')
	{!! $navbar !!}
@endsection

@section('slider')
	{!! $slider !!}
@endsection

@section('advantages_block')
	{!! $advantages !!}
@endsection

@section('bar')
	{!! $rightBar !!}
@endsection

@section('portfolio_block')
	{!! $portfolio !!}
@endsection