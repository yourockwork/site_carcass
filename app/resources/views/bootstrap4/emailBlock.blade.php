@if($email)
    <div class="b-information b-information--email">
        {!! $email->content !!}
    </div>
@endif