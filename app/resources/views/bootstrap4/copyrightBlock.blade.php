@if($copyright)
    <div class="b-copyright">
        {!! $copyright->content !!}
    </div>
@endif