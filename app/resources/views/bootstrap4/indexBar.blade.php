@if($sidemenu)
	@include(env('THEME').'.sideMenu',['sidemenu' => $sidemenu])
@endif

@if($banners)
	@include(env('THEME').'.banners',['banners' => $banners])
@endif

@if($articles)
	<div class="b-side">
		<div class="b-side__inner">
			<div class="b-side__name">{{trans('ru.articles_block')}}</div>
			<div class="b-side__items">
				@foreach($articles as $article)
					<div class="b-side__item">
						<div class="b-side-item__icon">
							<a href="#">
								<img class="img-responsive" src="{{asset(env('THEME'))}}/img/{{ ($article->img) ? $article->img->path : 'no-img-small.png' }}">
							</a>
						</div>
						<div class="b-side-item__text">
							<div class="b-side-item__name"><a href="{{route('articles.show',['alias' => $article->alias])}}">{{$article->title}}</a></div>
							<div class="b-side-item__date"><span>{{$article->created_at->format('d.m.y')}}</span></div>
						</div>
					</div>
				@endforeach
			</div><a class="b-btn-more" href="#">More</a>
		</div>
	</div>
@endif

@if($slogan)
	<div class="b-side-text">
		{!! $slogan->content !!}
	</div>
@endif
