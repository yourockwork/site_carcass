@foreach($banners as $banner)
    <div class="b-block">
        <div class="b-block__img">
            <a class="b-block__link" href="{{$banner->link}}">
                <img class="img-responsive" src="{{asset(env('THEME'))}}/img/{{ ($banner->img) ? $banner->img : 'no-img-side.png' }}" title="{{$banner->title}}" alt="{{$banner->alt}}">
            </a>
        </div>
    </div>
@endforeach
