{{-- #72 --}}
@foreach($comments as $comment)
	<li id="li-comment-{{ $comment->id }}" class="comment even {{ ($comment->user_id == $article->user_id) ?  'bypostauthor odd' : ''}}">
		<div id="comment-{{ $comment->id }}" class="comment-container">
			<div class="comment-author vcard">
				<img class="avatar" alt="" src="{{asset(env('THEME'))}}/img/no-avatar.png">
				<p>{{$comment->name}}</p>
			</div>
			<div class="comment-meta commentmetadata">
				<div class="intro">
					<div class="commentDate">
						<a href="#comment-{{ $comment->id }}">{{ is_object($comment->created_at) ? $comment->created_at->format('F d, Y \a\t H:i') : ''}}</a>
						@if($comment->parent_id && $comment->parent_id>0)
							в ответ на <a href="#comment-{{$comment->parent_id}}"> комментарий</a>
						@endif
					</div>
					<div class="commentNumber">#</div>
				</div>
				<div class="comment-body">
					{{$comment->text}}
				</div>
				<div class="reply group">
					<a class="comment-reply-link" href="#respond" onclick="return addComment.moveForm(&quot;comment-{{$comment->id}}&quot;, &quot;{{$comment->id}}&quot;, &quot;respond&quot;, &quot;{{$comment->article_id}}&quot;)">Reply</a>
				</div>
			</div>
		</div>
		@if(isset($articleGroup[$comment->id]))
			<ul class="children">
				@include(env('THEME').'.comments',['comments' => $articleGroup[$comment->id]])
			</ul>
		@endif
	</li>
@endforeach