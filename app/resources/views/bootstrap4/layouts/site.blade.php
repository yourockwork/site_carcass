<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="{{$meta_desc}}">
	<meta name="keywords" content="{{($keywords)?$keywords:'Page keywords'}}">
	<meta name="csrf-token" content="{{csrf_token()}}">
	<title>{{($pageHead)?$pageHead:'Page title'}}</title>

	<link rel="stylesheet" type="text/css" media="all" href="{{asset(env('THEME').'/css/libs.min.css')}}" />
	<link rel="stylesheet" type="text/css" media="all" href="{{asset(env('THEME').'/css/style.css')}}" />
	<link rel="stylesheet" type="text/css" media="all" href="{{asset(env('THEME').'/css/opensans.css')}}" />
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]><script src="{{asset(env('THEME').'/js/html5shiv.min.js')}}"></script><script src="{{asset(env('THEME').'/js/respond.min.js')}}"></script>
	<![endif]-->
</head>
<body>
<div class="modal fade" id="exampleModal" role="dialog" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title" id="exampleModalLabel">Напишите нам</h4>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<label class="control-label" for="recipient-name">Ваше имя:</label>
						<input class="form-control" id="recipient-name" type="text">
					</div>
					<div class="form-group">
						<label class="control-label" for="recipient-name">Ваш email:</label>
						<input class="form-control" type="email">
					</div>
					<div class="form-group">
						<label class="control-label" for="recipient-name">Телефон для связи:</label>
						<input class="form-control" type="tel">
					</div>
					<div class="form-group">
						<label class="control-label" for="message-text">Сообщение:</label>
						<textarea class="form-control" id="message-text"></textarea>
					</div>
					<div class="form-group">
						<div class="b-form-check">
							<input class="form-check-input" type="checkbox" id="checkbox">
							<label class="form-check-label" for="checkbox">Я соглашаюсь с обработкой моих персональных данных</label>
						</div>
					</div>
					<div class="modal-footer">
						<button class="btn btn-default" type="button" data-dismiss="modal">Закрыть</button>
						<button class="btn btn-success" type="button">Отправить</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="navbar navbar-inverse navbar-static-top" role="navigation">
	<div class="b-navbar-header">
		<div class="b-navbar-header__inner container">
			<button class="btn btn-xs btn-side btn-side--js" data-toggle="offcanvas"><i class="fa fa-reply"></i></button>
			<button class="navbar-toggle navbar-toggle--js" data-toggle="collapse" data-target=".navbar-collapse"><span class="sr-only">Navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
			<div class="row">
				<div class="col-lg-3 col-md-1 col-sm-3 col-xs-4">
					<div class="b-top_logo">
						<div class="g-d-cell g-align-middle"><a class="b-top_logo__icon" href="{{route('home')}}"></a></div>
						<div class="g-d-cell g-align-middle"><a class="b-top_logo__text visible-lg" href="{{route('home')}}">Block for your &quot;brand&quot;</a></div>
					</div>
				</div>
				<div class="col-lg-2 col-md-3 col-sm-2">
					@yield('email')
				</div>
				<div class="col-lg-3 col-md-4 col-sm-2">
					@yield('address')
				</div>
				<div class="col-lg-2 col-md-3 col-sm-2">
					@yield('phone')
				</div>
				<div class="col-lg-2 col-md-1 col-sm-3 col-xs-8 text-right">
					<div class="b-information b-information--feedback b-information--md"><a href="#" data-toggle="modal" data-target="#exampleModal"><strong>Обратная связь</strong></a></div>
				</div>
			</div>
		</div>
	</div>
	<!-- START NAVBAR -->
	@yield('navbar')
	<!-- END NAVBAR -->
</div>
<div class="container g-relative p-b__100px">
	<div class="row row-offcanvas row-offcanvas-right">
		<div class="{{($bar)?'col-lg-9 col-sm-9':'col-lg-12'}}">
			{{-- если не вставить этот блок то не будет работать отправка комментариев --}}
			<div class="wrap_result">
			</div>
			@yield('slider')
			@yield('advantages_block')
			<div class="b-content">
				@if($pageTitle)
					<div class="b-header1">
						<h1 class="b-h1">{{$pageTitle}}</h1>
						<div class="b-h1__sub"></div>
					</div>
				@endif
				@if($pageContent)
					{!! $pageContent !!}
				@endif
			</div>
			@yield('portfolio_block')
			@yield('articles')
			@yield('contacts_content')
			@yield('error_content')
		</div>
		@if($bar)
			@yield('bar')
		@endif
	</div>
	<button class="topBtn" onclick="topFunction()" title="наверх" id="topBtn">
		<i class="fa fa-arrow-up"></i>
	</button>
</div>
<footer class="footer">
	<div class="b-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-1 col-sm-3 col-xs-4">
					<div class="b-top_logo">
						<div class="g-d-cell g-align-middle"><a class="b-top_logo__icon" href="{{route('home')}}"></a></div>
						<div class="g-d-cell g-align-middle"><a class="b-top_logo__text visible-lg" href="{{route('home')}}">Block for your &quot;brand&quot;</a></div>
					</div>
				</div>
				<div class="col-lg-2 col-md-3 col-sm-2">
					@yield('email')
				</div>
				<div class="col-lg-3 col-md-4 col-sm-2">
					@yield('address')
				</div>
				<div class="col-lg-2 col-md-3 col-sm-2">
					@yield('phone')
				</div>
				<div class="col-lg-2 col-md-1 col-sm-3 col-xs-8 text-right hidden-lg">
					<div class="b-information b-information--feedback b-information--md"><a href="/contacts.html" data-toggle="modal" data-target="#exampleModal"><strong>Feedback</strong></a></div>
				</div>
				<div class="col-lg-2 col-md-12 col-sm-12 col-xs-8">
					@yield('copyright')
				</div>
			</div>
		</div>
	</div>
</footer>

<script src="{{asset(env('THEME').'/js/jquery.js')}}" crossorigin="anonymous"></script>
<script src="{{asset(env('THEME').'/js/libs.js')}}" crossorigin="anonymous"></script>

</body>
</html>
