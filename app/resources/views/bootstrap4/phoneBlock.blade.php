@if($phone)
    <div class="b-information b-information--phone">
        {!! $phone->content !!}
    </div>
@endif