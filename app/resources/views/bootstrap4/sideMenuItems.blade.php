@foreach($items as $item)
	{{-- аккуратнее с кавычками, из-за них могут быть баги --}}
	<a class="list-group-item{{ ($item->hasChildren()) ? ' item-parent collapsed' : '' }}{{ (URL::current() == $item->url()) ? ' active' : '' }}" href="{{ ($item->hasChildren()) ? '#collapse'.$item->id : '' }}"{{ ($item->hasChildren()) ? ' data-toggle=collapse' : '' }}>{{$item->title}}</a>
	@if($item->hasChildren())
		<div class="panel-collapse collapse {{ (URL::current() == $item->url()) ? 'in' : '' }}" id="collapse{{$item->id}}">
			<div class="list-group-inner">
				@include(env('THEME').'.sideMenuChildrenItems',['items' => $item->children()])
			</div>
		</div>
	@endif
@endforeach
