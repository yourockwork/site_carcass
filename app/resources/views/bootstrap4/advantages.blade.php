@if($advantages)
	<div class="b-advantages">
		<div class="b-advantages__inner">
			<div class="row">
				@foreach($advantages as $advantage)
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="b-advantages__item">
							<div class="b-advantages__icon">
								<a href="{{$advantage->link}}">
									<i class="fa fa-{{$advantage->icon}}"></i>
								</a>
							</div>
							<div class="b-advantages__text">
								<a href="{{$advantage->link}}">{{$advantage->text}}</a>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
@endif
