@if($menu)
	<div class="navbar-collapse collapse navbar-collapse--background">
		<div class="container">
			<ul class="nav navbar-nav">
				{{-- вызываем и передаем в шаблон только родительские пункты меню --}}
				@include(env('THEME').'.topMenuItems',['items' => $menu->roots()])
			</ul>
			<form class="navbar-form navbar-right">
				<div class="g-d-flex">
					<input class="form-control m-r__5px" placeholder="Поиск">
					<button class="btn btn-success">Найти</button>
				</div>
			</form>
		</div>
	</div>
@endif