<?php

return [
	'comments' => 'Комментарий|Комментария|Комментариев',
	'articles_block' => 'Статьи',
	'latest_projects' => 'Последние работы',
	'latest_comments' => 'Последние коментарии',
	'from_blog' => 'Последние материалы',
	'read_more' => 'Подробнее',
	'articles_no' => 'Записей нет',
];