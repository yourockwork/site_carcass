<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
// используем только маршрут index, псевдоним name
Route::resource('/','IndexController',[
	'only' => 'index',
	'names' => ['index' => 'home']
]);


Route::resource('portfolios','PortfolioController',[
	'parameters' => [
		'portfolios' => 'alias'
	]
]);


Route::resource('articles','ArticlesController',[
	'parameters' => [
		'articles' => 'alias'
	]
]);

//Route::get('articles/cat/{cat_alias?}', ['uses' => 'ArticlesController@index', 'as' => 'articlesCat'])->where('cat_alias','[\w-]+');
Route::get('articles/cat/{cat_alias?}', ['uses' => 'ArticlesController@index', 'as' => 'articlesCat']);

Route::resource('comment','CommentController',[
	'only' => 'store'
]);

Route::match(['get','post'],'/contacts',['uses'=>'ContactsController@index','as'=>'contacts']);

// формируем список маршрутов для аутентификации
Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function (){
	Route::get('/',['uses' => 'Admin\IndexController@index','as' => 'adminIndex']);
	// Route::resource('/articles','Admin/ArticlesController ');
	Route::resource('/articles', 'Admin\ArticlesController',[
		'names' => [
			'store' => 'admin.articles.store',
			'index' => 'admin.articles.index',
			'create' => 'admin.articles.create',
			'destroy' => 'admin.articles.destroy',
			'update' => 'admin.articles.update',
			'show' => 'admin.articles.show',
			'edit' => 'admin.articles.edit',
		],
	]);
	Route::resource('/permissions', 'Admin\PermissionsController',[
		'names' => [
			'store' => 'admin.permissions.store',
			'index' => 'admin.permissions.index',
			'create' => 'admin.permissions.create',
			'destroy' => 'admin.permissions.destroy',
			'update' => 'admin.permissions.update',
			'show' => 'admin.permissions.show',
			'edit' => 'admin.permissions.edit',
		],
	]);
	Route::resource('/menus', 'Admin\MenusController',[
		'names' => [
			'store' => 'admin.menus.store',
			'index' => 'admin.menus.index',
			'create' => 'admin.menus.create',
			'destroy' => 'admin.menus.destroy',
			'update' => 'admin.menus.update',
			'show' => 'admin.menus.show',
			'edit' => 'admin.menus.edit',
		],
	]);
	Route::resource('/users', 'Admin\UsersController',[
		'names' => [
			'store' => 'admin.users.store',
			'index' => 'admin.users.index',
			'create' => 'admin.users.create',
			'destroy' => 'admin.users.destroy',
			'update' => 'admin.users.update',
			'show' => 'admin.users.show',
			'edit' => 'admin.users.edit',
		],
	]);
});

